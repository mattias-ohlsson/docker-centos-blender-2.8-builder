FROM centos

LABEL maintainer="mattias.ohlsson@inprose.com"

ENV HOME /root

RUN yum update -y 

RUN yum -y install git

RUN mkdir $HOME/blender-git
WORKDIR $HOME/blender-git
RUN git clone https://git.blender.org/blender.git
WORKDIR $HOME/blender-git/blender
RUN git checkout blender2.8
RUN git submodule update --init --recursive
RUN git submodule foreach git checkout master
RUN git submodule foreach git pull --rebase origin master
